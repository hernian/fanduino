# Fanduino

USB扇風機の風量を調整する。

![概観](images/fanduino.jpg "概観")

## 仕様
- 8段階の風量調整
- ONされたときには最大風量
- ONのときに Pro MicroのTX LEDを点灯。OFFのときにTX LEDを消灯。
- Pro MicroのTX LEDの輝度を風量に合わせて8段階表示（あまり区別がつかない）

## 作り方

Table: Pro Microのピンの使用目的

|#|pin|使用目的|I/O|備考|
|:--|:--|:--|:--|:--|
|1|D9|MOS-FETのゲート制御|出力|PWMで出力する|
|2|D14|風量UP|入力|ファームでプルアップ要|
|3|D15|風量DOWN|入力|ファームでプルアップ要|
|4|D16|ON/OFF|入力|ファームでプルアップ要|
