/// @file button.h

#pragma once

#include <Arduino.h>


class Button {
  public:
    void    Begin(uint8_t pinIn);
    void    Update();
    bool    IsPressed() const;
    void    Reset();
  private:
    uint8_t _pinIn;
    uint8_t _countDebounce;
    uint8_t _rawStat;
    bool    _btnStat;
};


