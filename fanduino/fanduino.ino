/// @file fanduino.ino
#include <MsTimer2.h>
#include "button.h"

const uint8_t PIN_MOSFET_GATE  = 9;

const uint8_t PIN_BUTTON_DOWN   = 14;
const uint8_t PIN_BUTTON_UP     = 15;
const uint8_t PIN_BUTTON_ONOFF  = 16;


const uint8_t FANSPEED_MAX = 10;
const uint8_t FANSPEED_MIN = 2;

const uint8_t SCALE_LED_SPEED = 10;
const uint8_t COUNT_LED_MAX = FANSPEED_MAX * FANSPEED_MAX;

static uint8_t s_fanSpeed;
static Button s_btnOnOff;
static Button s_btnUp;
static Button s_btnDown;

static uint8_t s_countLedTx;
static uint8_t s_countLed;

static uint8_t s_flagTimer;

void onTimer() {
  s_flagTimer = 1;
}

void SetLedTx(uint8_t speed){
  s_countLedTx = speed * speed;
}

void UpdateLed(uint8_t pin, uint8_t count) {
  if (count > s_countLed) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }
  else {
    pinMode(pin, INPUT_PULLUP);
  }
}

uint8_t getVrefFromFanSpeed(uint8_t fanSpeed) {
  uint16_t fs = fanSpeed;
  uint16_t temp = fs * 255 / FANSPEED_MAX;
  uint8_t vref = static_cast<uint8_t>(temp);
  return vref;
}

void InitFanSpeed() {
  s_fanSpeed = 0;
  SetLedTx(s_fanSpeed);
  uint8_t vref = getVrefFromFanSpeed(s_fanSpeed);
  pinMode(PIN_MOSFET_GATE, OUTPUT);
  analogWrite(PIN_MOSFET_GATE, vref);
}

void SetFanSpeed(uint8_t fanSpeed) {
  uint8_t temp = min(fanSpeed, FANSPEED_MAX);
  s_fanSpeed = max(temp, FANSPEED_MIN);
  SetLedTx(s_fanSpeed);
  uint8_t vref = getVrefFromFanSpeed(s_fanSpeed);
  analogWrite(PIN_MOSFET_GATE, vref);
}

void ToggleFanSpeed(){
  s_fanSpeed = (s_fanSpeed == 0) ? FANSPEED_MAX: 0;
  SetLedTx(s_fanSpeed);
  uint8_t vref = getVrefFromFanSpeed(s_fanSpeed);
  analogWrite(PIN_MOSFET_GATE, vref);
}

void UpFanSpeed() {
  if (s_fanSpeed == 0){
    return;
  }
  if (s_fanSpeed >= FANSPEED_MAX) {
    return;
  }
  SetFanSpeed(s_fanSpeed + 1);
}

void DownFanSpeed() {
  if (s_fanSpeed == 0){
    return;
  }
  if (s_fanSpeed <= FANSPEED_MIN) {
    return;
  }
  SetFanSpeed(s_fanSpeed - 1);
}

void InitButton() {
  s_btnOnOff.Begin(PIN_BUTTON_ONOFF);
  s_btnUp.Begin(PIN_BUTTON_UP);
  s_btnDown.Begin(PIN_BUTTON_DOWN);
}

void ButtonTask() {
  s_btnOnOff.Update();
  s_btnUp.Update();
  s_btnDown.Update();
  if (s_btnOnOff.IsPressed()){
    s_btnOnOff.Reset();
    ToggleFanSpeed();
  }
  if (s_btnUp.IsPressed()) {
    s_btnUp.Reset();
    UpFanSpeed();
  }
  if (s_btnDown.IsPressed()) {
    s_btnDown.Reset();
    DownFanSpeed();
  }
}


void setup() {
  InitButton();
  InitFanSpeed();

  s_flagTimer = 0;
  MsTimer2::set(1, onTimer);
  MsTimer2::start();
}


void loop() {
  if (s_flagTimer) {
    s_flagTimer = 0;
    ButtonTask();
  }
  ++s_countLed;
  if (s_countLed >= COUNT_LED_MAX){
    s_countLed = 0;
  }
  UpdateLed(LED_BUILTIN_RX, 0);
  UpdateLed(LED_BUILTIN_TX, s_fanSpeed);
}


