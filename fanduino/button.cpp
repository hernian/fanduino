/// @file button.cpp

#pragma once

#include "button.h"

static const uint8_t COUNT_DEBOUNCE_MAX = 5;        // 5msec

void Button::Begin(uint8_t pinIn){
    _pinIn = pinIn;
    _countDebounce = 0;
    _rawStat = HIGH;
    _btnStat = false;
    pinMode(_pinIn, INPUT_PULLUP);
}


void Button::Update(){
    _rawStat = digitalRead(_pinIn);
    if (_rawStat == LOW){
        if (_countDebounce < COUNT_DEBOUNCE_MAX){
            ++_countDebounce;
            if (_countDebounce >= COUNT_DEBOUNCE_MAX){
                _btnStat = true;
            }
        }
    }
    else {
        _countDebounce = 0;
        _btnStat = false;
    }
}


bool Button::IsPressed() const {
    return _btnStat;
}


void Button::Reset() {
    _btnStat = false;
}

